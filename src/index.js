import React from 'react';
import ReactDOM from 'react-dom';
/* APP */
import App from './routes/App';

ReactDOM.render(<App />, document.getElementById('app'));
