import {useState, useEffect} from 'react';
// import axios from 'axios';
import apiMockup from './apiMockup';

const useGoogleAddress = () => {
  const [map, setMap] = useState({});
  /* const API = `https://maps.googleapis.com/maps/api/geocode/json?address=${address},+Mountain+View,+CA&key=${process.env.GMAPS_API_KEY}`; */

  useEffect(() => {
    /* const response = await axios(API);
    setMap(response.data.results[0].geometry.location); */
    setMap(apiMockup.results[0].geometry.location);
  }, []);
  return map;
}

export default useGoogleAddress;