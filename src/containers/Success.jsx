import React, {useContext} from 'react';
/* CONTEXT */
import AppContext from '../context/AppContext';
/* HOOKS */
import useGoogleAddress from '../hooks/useGoogleAddress';
/* COMPONENTS */
import Map from '../components/Map';
/* CSS */
import '../styles/components/Success.css';

const Success = () => {
  const {state} = useContext(AppContext);
  const {buyer} = state;
  // const address = useGoogleAddress(buyer[0].address);
  const location = useGoogleAddress();
  return (
    <div className="Success">
      <dic className="Success-content">
        <h2>{`${buyer.name}, Gracias por tu compra`}</h2>
        <span>Tu pedido llegará en X Días a tu dirección</span>
        <div className="Success-map">
          <Map data={location} /> 
        </div>
      </dic>
    </div>
  );
};

export default Success;